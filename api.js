const fetch = require("node-fetch");
const queryBuilder = require('./build_query');

class AXIE {
    constructor(address = null, debug = true) {
        this._version = "0.0.1";
        this._host = "https://api.axieinfinity.com/";

        if (typeof address !== "string") {
            throw new Error("address must be a string");
        }
        this.address = address;
        this._debug = debug;

        if (this._debug) {
            console.info("Thank you for using AXIE.js v" + this._version)
        }
    }

    async isCharmEnabled() {
        let charm = await (await fetch(this._host + 'v1/battle/battle/check-charm/' + this.address)).json();
        return Promise.resolve(charm);
    }

    async getBattleTeams() {
        let teams = await (await fetch(this._host + 'v1/battle/teams/?no_limit=1&offset=0&count=1500&address=' + this.address)).json();
        return Promise.resolve(teams);
    }

    async getAccount() {
        let account = await (await fetch(this._host + 'v1/battle/accounts/' + this.address)).json();
        return Promise.resolve(account);
    }

    async getAxie(axieId) {
        let axie = await (await fetch(this._host + 'v1/axies/' + axieId)).json();
        return Promise.resolve(axie);
    }

    async getBattleTeam(teamId) {
        let team = await (await fetch(this._host + 'v1/battle/teams/' + teamId)).json();
        return Promise.resolve(team);
    }

    async getActivityPoints(axieId) {
        let activityPoints = await (await fetch(this._host + 'v1/battle/battle/activity-point?axieId=' + axieId)).json();
        return Promise.resolve(activityPoints);
    }

    async canBattleTeamFight(teamId) {
        let team = await this.getBattleTeam(teamId);
        let activityPoints = [];
        team.teamMembers.forEach(teamMember => {
            activityPoints.push(this.getActivityPoints(teamMember.axieId));
    });

        let activityPointsResult = await Promise.all(activityPoints);

        activityPointsResult.map(x => {
            x[0].ready = parseInt(x[0].activityPoint / 240);
        return x;
    });

        return Promise.resolve({
            teamId: teamId,
            canFight: !(activityPointsResult[0][0].ready === activityPointsResult[1][0].ready &&
                activityPointsResult[0][0].ready === activityPointsResult[1][0].ready &&
                activityPointsResult[0][0].ready === 0)
        })

    }

    static async findAxies(page = 1,  for_sale = null, sorting = null, title = null, breedable = null, pureness = null, num_mystic = null, type = null, stage = null) {
        let query = queryBuilder({
            offset: (page -1) * 10,
            page,
            for_sale,
            sorting,
            title,
            breedable,
            pureness,
            num_mystic,
            "class":type,
            stage
        });
        let axies = await (await fetch('https://axieinfinity.com/api/v2/axies?' + query)).json();
        return Promise.resolve(axies);
    }

}

module.exports = AXIE;