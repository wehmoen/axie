const AXIE = require("./api");
let axie = new AXIE("0x07b37e682b100208c6288f2f121298a4d60d103c", false);

axie.getAccount().then(account => {
    console.log("Hello",account.name);
    axie.getBattleTeams().then(teams => {
        console.log("You have", teams.total, "teams:");
        console.log("Checking battle readiness...");
        console.log("=====");
        let battleStatus = [];

        teams.teams.forEach(team => {
            battleStatus.push(axie.canBattleTeamFight(team.teamId));
        });

        Promise.all(battleStatus).then(status => {
            status.forEach((battleTeam) => {
                let team = teams.teams.filter((team) => team.teamId === battleTeam.teamId);
                console.log(">",team[0].name);
                console.log("Can fight:", battleTeam.canFight.toString());
                console.log("=====");
            });
            return Promise.resolve();
        }).then(() => {
            axie.isCharmEnabled().then(charm => {
                if (charm.isCharmActivated) {
                    console.log("Great! Your charm is activated!");
                } else {
                    console.log("You should activate your charm!");
                }
            })
        })

    })
});
